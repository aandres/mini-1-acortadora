import urllib.parse
import random
import string
from webApp import webApp

form = """
    <form action="" method="POST">
        <p>URL: <input type="text" name="URL"></p>
        <p><input type="submit" value="Submit"></p>
    </form>
"""

page = """
            <html>
                <body>
                    <h3>App acortadora de URLs</h3>
                    <p>{form}</p>
                    <h5>URL que mandas acortar: {cuerpo}</h5>
                    <ul>
                    {dic}
                    </ul>
                </body>
            </html>
            """

not_page = """
            <html>
                <body>
                    <h1>NOT FOUND - Recurso no disponible</h1>
                </body>
            </html>
            """


class ShortenerApp(webApp):
    def __init__(self, hostname, port):
        self.contents = {}
        super().__init__(hostname, port)

    def parse(self, request):
        metodo = request.split(" ", 2)[0]
        recurso = request.split(" ", 2)[1]
        cuerpo = request.split('\r\n\r\n', 1)[1]
        return metodo, recurso, cuerpo

    def process(self, parsedRequest):

        metodo, recurso, cuerpo = parsedRequest

        if metodo == "GET":
            if recurso == "/":
                httpCode = "200 OK"
                htmlBody = page.format(form=form, cuerpo=cuerpo, dic=self.contents)
            else:
                rec_flt = recurso.split('/')[1]
                print(rec_flt)
                print(self.contents)
                if rec_flt in self.contents:
                    url_ir = self.contents[rec_flt]
                    httpCode = "301 Moved Permanently"
                    htmlBody = "<html><body>" \
                               + "<meta http-equiv='refresh' content='0; URL=" + str(url_ir) + "'></body></html>"
                else:
                    httpCode = "404 Not Found"
                    htmlBody = "<html><body><h1>Recurso no encontrado</h1></body></html>"

        elif metodo == "POST":
            cuerpo = cuerpo.split("=")[1]
            cuerpo = urllib.parse.unquote(cuerpo)
            if not (cuerpo.startswith("http://") or cuerpo.startswith("https://")):
                cuerpo = "https://" + cuerpo
            print(cuerpo)
            if cuerpo not in self.contents.values():
                self.create_urlrandom()
                self.add_dic(cuerpo)

            httpCode = "200 OK"
            url_base = 'http://localhost:1234/'
            print(recurso)
            htmlBody = f"<html><body>URL Original: <a href='{cuerpo}'>{cuerpo}</a><br>URL Acortada: <a href='{url_base + self.urlrandom}'>{url_base + self.urlrandom} </a></body></html>"

        else:
            httpCode = "404 Not Found"
            htmlBody = not_page
        return httpCode, htmlBody

    def create_urlrandom(self):
        characters = string.ascii_letters + string.digits
        sec_random = ''.join(random.choice(characters) for i in range(5))
        self.urlrandom = sec_random
        print(self.urlrandom)
        return self.urlrandom

    def add_dic(self, cuerpo):
        self.contents[self.urlrandom] = cuerpo
        print(self.contents)
        return self.contents


if __name__ == "__main__":
    ShortenerApp = ShortenerApp("localhost", 1234)


